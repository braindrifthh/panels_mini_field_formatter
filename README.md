# Mini panels field formatter

Just like the module [Views field formatter](https://www.drupal.org/project/views_field_formatter) this module
provides a field formatter for every field. This formatter renderes a field using a mini panel.

The mini panel used as formatter can have three required contexts (in the following fixed order).

1. Entity: The Entity where the the formatter is used. (eg. Node, User, Taxonomy term, ...)
2. String: The value of the current field item
3. String: The delta of the current field item

The formatter can be configured to render all field items in one mini panel or to render a mini panel form each item.

#### Dependencies

- [Mini panel adv](https://bitbucket.org/axuet/panels_mini_adv)